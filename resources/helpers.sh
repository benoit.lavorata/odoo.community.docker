#!/bin/bash
###############
### HELPERS ###
###############
# COLORS
__NC=$(echo -en '\033[0m')
__RED=$(echo -en '\033[00;31m')
__GREEN=$(echo -en '\033[00;32m')
__YELLOW=$(echo -en '\033[00;33m')
__BLUE=$(echo -en '\033[00;34m')
__MAGENTA=$(echo -en '\033[00;35m')
__PURPLE=$(echo -en '\033[00;35m')
__CYAN=$(echo -en '\033[00;36m')
__LIGHTGRAY=$(echo -en '\033[00;37m')
__LRED=$(echo -en '\033[01;31m')
__LGREEN=$(echo -en '\033[01;32m')
__LYELLOW=$(echo -en '\033[01;33m')
__LBLUE=$(echo -en '\033[01;34m')
__LMAGENTA=$(echo -en '\033[01;35m')
__LPURPLE=$(echo -en '\033[01;35m')
__LCYAN=$(echo -en '\033[01;36m')
__WHITE=$(echo -en '\033[01;37m')
__GREY=$(echo -en '\033[01;30m')

# SET FORMATS
__COLOR_DATE=$__BLUE
__COLOR_INTRO=$__BLUE
__COLOR_SUB_INTRO=$__YELLOW
__COLOR_INFO=$__BLUE
__COLOR_WARNING=$__YELLOW
__COLOR_DEBUG=$__GREY
__COLOR_ERROR=$__RED
__COLOR_SUCCESS=$__GREEN
__COLOR_SECTION=$__WHITE
__COLOR_LOG=$__LIGHTGRAY
__COLOR_PROMPT=$__YELLOW
__COLOR_PROMPT_ANSWER=$__LCYAN

# DEFINE LOG FUNCTION
__DATE='date +%Y%m%d-%H%M%S'
__YEAR=`date +%Y`
__PREFIX=$(echo -en $__COLOR_DATE`$__DATE`" | "$__NC)
__INDENT="  "
__LINE="-------------------------------------------------"

function _intro {
    echo -e "${__COLOR_INTRO}${__LINE}"
    echo -e "SCRIPT:${__COLOR_LOG} ${SCRIPT_NAME} ${__COLOR_INTRO}"
    echo -e "INFO:${__COLOR_LOG} ${SCRIPT_DESCRIPTION}${__COLOR_INTRO}"
    echo -e "AUTHOR:${__COLOR_LOG} ${SCRIPT_AUTHOR}, ${__YEAR} ${__COLOR_INTRO}"
    echo -e "${__LINE}${__NC}"
}

function _outro {
    echo -e "${__COLOR_INTRO}"
    echo -e ${__LINE}
    echo -e "Have a good day,"
    echo -e "$SCRIPT_AUTHOR."
    echo -e ${__LINE}
    echo -e "${__NC}"
}

function _br {
    echo " "
}

function _hr {
    _br
    echo -e "${__NC}${__LINE}${__NC}"
    _br
}

function _log {
    echo -e ${__PREFIX}${__COLOR_LOG}"$1"${__NC}
}
function _log1 {
    _log "${__INDENT}$1"
}
function _log2 {
    _log1 "${__INDENT}$1"
}
function _log3 {
    _log2 "${__INDENT}$1"
}
function _log4 {
    _log3 "${__INDENT}$1"
}
function _error {
    _log "${__COLOR_ERROR}[ERROR]${__NC} $1"
}
function _error1 {
    _log1 "${__COLOR_ERROR}[ERROR]${__NC} $1"
}
function _error2 {
    _log2 "${__COLOR_ERROR}[ERROR]${__NC} $1"
}
function _error3 {
    _log3 "${__COLOR_ERROR}[ERROR]${__NC} $1"
}
function _error4 {
    _log4 "${__COLOR_ERROR}[ERROR]${__NC} $1"
}
function _success {
    _log "${__COLOR_SUCCESS}[SUCCESS]${__NC} $1"
}
function _success1 {
    _log1 "${__COLOR_SUCCESS}[SUCCESS]${__NC} $1"
}
function _success2 {
    _log2 "${__COLOR_SUCCESS}[SUCCESS]${__NC} $1"
}
function _success3 {
    _log3 "${__COLOR_SUCCESS}[SUCCESS]${__NC} $1"
}
function _success4 {
    _log4 "${__COLOR_SUCCESS}[SUCCESS]${__NC} $1"
}

function _warning {
    _log "${__COLOR_WARNING}[WARNING]${__NC} $1"
}
function _warning1 {
    _log1 "${__COLOR_WARNING}[WARNING]${__NC} $1"
}
function _warning2 {
    _log2 "${__COLOR_WARNING}[WARNING]${__NC} $1"
}
function _warning3 {
    _log3 "${__COLOR_WARNING}[WARNING]${__NC} $1"
}
function _warning4 {
    _log4 "${__COLOR_WARNING}[WARNING]${__NC} $1"
}

function _info {
    _log "${__COLOR_INFO}[INFO]${__NC} $1"
}
function _info1 {
    _log1 "${__COLOR_INFO}[INFO]${__NC} $1"
}
function _info2 {
    _log2 "${__COLOR_INFO}[INFO]${__NC} $1"
}
function _info3 {
    _log3 "${__COLOR_INFO}[INFO]${__NC} $1"
}
function _info4 {
    _log4 "${__COLOR_INFO}[INFO]${__NC} $1"
}

function _ensure_odoo_user_owns_volume {
    #_log3 "Ensure Odoo user owns volumes..."
    # Make sure the folder exists
    _VOLUME=$1
    _ODOO_USER=$2
    if [ -d "$_VOLUME" ]; then
        # Check if the volume has been mounted read-only
        MOUNT_TYPE=$( cat /proc/mounts | grep "\s$_VOLUME\s" | \
            awk '{print tolower(substr($4,0,3))}' )

        if [ "$MOUNT_TYPE" != 'ro' ]; then
            # Set target user as owner
            chown "$_ODOO_USER":"$_ODOO_USER" "$_VOLUME"
            _success4 "Volume $1 now belongs to user $ODOO_USER"
        else
            _warning4 "Read-only volume: $_VOLUME"
        fi
    else
        _warning4 "Volume does not exist: $_VOLUME"
    fi
}

function _update_odoo_conf_params {
    _ODOO_CONF_FILE_PATH=$1

    _log2 "Update $_ODOO_CONF_FILE_PATH file"

    cp $_ODOO_CONF_FILE_PATH ${_ODOO_CONF_FILE_PATH}.backup     
    cp $_ODOO_CONF_FILE_PATH ${_ODOO_CONF_FILE_PATH}.new
    
    if [ -f "${_ODOO_CONF_FILE_PATH}.tmp" ]; then
    	rm ${_ODOO_CONF_FILE_PATH}.tmp
    fi

    while read -r env_var; do
        # Remove "ODOO_" from ENV variable and convert to lowercase
        ODOO_PARAM=${env_var:5}
        ODOO_PARAM=${ODOO_PARAM,,}

        # Get the value of the corresponding ENV variable and escape slashes
        val=${!env_var}
        val=$( echo "$val" )
        #val=$( echo "$val" | sed 's/\//\\\//g' )

        #_log2 "Handle param $ODOO_PARAM"
        ODOO_PARAM_FOUND=0
        touch ${_ODOO_CONF_FILE_PATH}.tmp

        while read line
        do
            if [[ $line == "${ODOO_PARAM} = "* ]] ; then
                _log3 "Substitute $ODOO_PARAM"
                ODOO_PARAM_FOUND=1
                
                echo "${ODOO_PARAM} = ${val}" >> ${_ODOO_CONF_FILE_PATH}.tmp
            else
                #_log3 "App $ODOO_PARAM"
                echo "$line" >> ${_ODOO_CONF_FILE_PATH}.tmp
            fi
        done < ${_ODOO_CONF_FILE_PATH}.new

        if [[ "$ODOO_PARAM_FOUND" == 0 ]]; then
            _log3 "Append $ODOO_PARAM"
            echo "${ODOO_PARAM} = ${val}" >> ${_ODOO_CONF_FILE_PATH}.tmp
        fi
        mv ${_ODOO_CONF_FILE_PATH}.tmp ${_ODOO_CONF_FILE_PATH}.new

        # Unset the environment variable for security purpose
        unset "$env_var"
    done <<< "$( printenv | grep '^ODOO_' | sed 's/=.*//g' )"
    cat ${_ODOO_CONF_FILE_PATH}.new > ${_ODOO_CONF_FILE_PATH}
    rm ${_ODOO_CONF_FILE_PATH}.new
    _success4 "Updated conf file"
}

function _setup_ssh_key {
    _ODOO_USER=$1

    _log2 "Setup SSH Key"
    # SSH config folder in $HOME folder of target user
    SSH_FOLDER=$( getent passwd "$_ODOO_USER" | cut -d: -f6 )/.ssh

    # SSH config folder already exists when container has been restarted
    if [ ! -d "$SSH_FOLDER" ]; then
        # Create SSH config folder
        _log3 "Create ssh folder"
        sudo -i -u "$_ODOO_USER" mkdir "$SSH_FOLDER"

        # Copy SSH private key from /opt/odoo/ssh
        _log3 "Copy SSH private key"
        sudo -i -u "$_ODOO_USER" cp /opt/odoo/ssh/id_rsa "$SSH_FOLDER"

        _log3 "Scan Github Key"
        # Hide ssh-keyscan stderr output since it's actually log message
        ssh-keyscan github.com 2> /dev/null | \
            sudo -i -u "$_ODOO_USER" tee "$SSH_FOLDER/known_hosts" > /dev/null

        # Bind SSH key to GitHub host
        _log3 "Bind SSH key to github host"
        echo "host github.com
                HostName github.com
                User git
                IdentityFile $SSH_FOLDER/id_rsa" | \
            sudo -i -u "$_ODOO_USER" tee "$SSH_FOLDER/config" > /dev/null

        # Secure SSH key
        _log3 "Set permissions on ssh key (400)"
        chmod 400 "$SSH_FOLDER/id_rsa"

        _success4 "SSH Key properly set"
    else
        _success3 "SSH folder already exists, container was just restarted"
    fi
}


function _download_marketplace_addons {
    _DEPENDENCIES_PATH=$1
    _DESTINATION_PATH=$2
    _FORCE=$3

    if [ -z ${_FORCE+x} ]; then 
        _FORCE=0
    fi

    _log2 "Download Marketplace Addons from $_DEPENDENCIES_PATH to $_DESTINATION_PATH"
    if [ -f "$_DEPENDENCIES_PATH" ]; then
        if [ -d "$_DESTINATION_PATH" ]; then

            # Read each line
            while IFS= read -r line
            do
                if [[ $line = \#* ]] ; then
                    #_log3 "Skip commented line"
		            cd $_DESTINATION_PATH
                else
                    _log3 "Checking dependency: $line"
                    cd $_DESTINATION_PATH

                    IFS=' ' # space is set as delimiter
                    read -ra ADDR <<< "$line" # str is read into an array as tokens separated by IFS

                    #echo "${#ADDR[@]}"
                    MOD_NAME=${ADDR[0]}
                    MOD_VERSION=${ADDR[1]}
                    
                    if [ -z ${MOD_NAME} ]; then 
                        _warning4 "Module name is unset, skip"; 
                    else 
                        REPO_DEST=$_DESTINATION_PATH/$MOD_NAME

                        if [ -z ${MOD_VERSION} ]; then 
                            _warning4 "Module version is unset, skip"; 
                        else
                            if [[ "$_FORCE" == 1 ]]; then
                                if [ -d "$REPO_DEST" ]; then
                                    _log4 "force mode enabled, delete existing repo"
                                    rm -rf $REPO_DEST
                                fi
                            fi
                            
                            # pull the repo if needed
                            if [ -d "$REPO_DEST" ]; then
                                _warning4 "$REPO_DEST repo already deployed, force mode disabled, skip"
                            else
                                MOD_URL="https://www.odoo.com/apps/modules/${MOD_VERSION}/${MOD_NAME}/"
                                _log4 " Odoo Apps URL: $MOD_URL"
                                
                                MOD_ZIP_URL=$(curl -s $MOD_URL |
                                    grep -Eoi '<a [^>]+>' | 
                                    grep -Eo 'href="[^\"]+"' | 
                                    grep 'download' | 
                                    grep '.zip' |
                                    grep 'https' |
                                    grep -Eo '(http|https)://[^"]+')
                                _log4 " Zip URL: $MOD_ZIP_URL"

                                # Get the repo
                                if curl --head --silent --fail $MOD_ZIP_URL >/dev/null 2>&1 ;
                                then
                                    _log " Download to: ${_DESTINATION_PATH}/${MOD_NAME}.zip"
                                    curl $MOD_ZIP_URL -o ${_DESTINATION_PATH}/${MOD_NAME}.zip
                                    unzip -q -f ${MOD_NAME}.zip
                                    rm ${MOD_NAME}.zip
                                    _success4 "Downloaded $MOD_NAME to $REPO_DEST"
                                else
                                    _error4 "The Zip URL of the repo does not exist"
                                fi
                            fi
                        fi
                    fi
                fi

                unset MOD_VERSION
                unset MOD_URL
                unset MOD_NAME
                unset REPO_DEST
            done < "$_DEPENDENCIES_PATH"
        else
            _error3 "$_DESTINATION_PATH does not exist"
        fi
    else
        _error3 "$_DEPENDENCIES_PATH does not exist"
    fi
    unset _FORCE
    unset _PULL
    unset _DEPENDENCIES_PATH
    unset _DESTINATION_PATH
}

function _download_git_addons {
    _DEPENDENCIES_PATH=$1
    _DESTINATION_PATH=$2
    _PULL=$3
    _FORCE=$4

    if [ -z ${_FORCE+x} ]; then 
        _FORCE=0
    fi

    if [ -z ${_PULL+x} ]; then 
        _PULL=0
    fi

    _log2 "Download Git / OCS Addons from $_DEPENDENCIES_PATH to $_DESTINATION_PATH"
    if [ -f "$_DEPENDENCIES_PATH" ]; then
        if [ -d "$_DESTINATION_PATH" ]; then

            # Read each line
            while IFS= read -r line
            do
                if [[ $line = \#* ]] ; then
                    #_log3 "Skip commented line"
                    cd $_DESTINATION_PATH
                else
                    _log3 "Checking dependency: $line"
                    cd $_DESTINATION_PATH

                    IFS=' ' # space is set as delimiter
                    read -ra ADDR <<< "$line" # str is read into an array as tokens separated by IFS

                    #echo "${#ADDR[@]}"
                    MOD_NAME=${ADDR[0]}
                    MOD_URL=${ADDR[1]}
                    MOD_BRANCH=${ADDR[2]}
                    
                    if [ -z ${MOD_NAME+x} ]; then 
                        _warning4 "Module name is unset, skip"; 
                    else 
                        REPO_DEST=$_DESTINATION_PATH/$MOD_NAME

                        if [ -z ${MOD_URL} ]; then 
                            MOD_URL="https://github.com/oca/$MOD_NAME.git"
                            _log4 "Module url is unset, set to default OCA module"; 
                        fi
                        
                        if [[ "$_FORCE" == 1 ]]; then
                            if [ -d "$REPO_DEST" ]; then
                                _log4 "force mode enabled, delete existing repo"
                                rm -rf $REPO_DEST
                            fi
                        fi
                        
                        # pull the repo if needed
                        if [ -d "$REPO_DEST" ]; then
                            if [[ "$_PULL" == 1 ]]; then
                                # Pull repo
                                _log4 "Pull repo"
                                cd $REPO_DEST
                                git pull
                                cd $_DESTINATION_PATH
                                _success4 "Pulled repo"
                            else
                                _warning4 "$REPO_DEST repo already deployed, pull mode disabled, skip"
                            fi
                        else
                            # Get the repo
                            if curl --head --silent --fail $MOD_URL >/dev/null 2>&1 ;
                            then
                                if [ -z ${MOD_BRANCH} ]; then 
                                    _log4 "  no branch set, use default"; 
                                    git clone $MOD_URL $REPO_DEST
                                else 
                                    _log4 "  branch set to |$MOD_BRANCH|"; 
                                    git clone --branch $MOD_BRANCH $MOD_URL $REPO_DEST
                                fi
                                _success4 "Cloned $MOD_NAME to $REPO_DEST"
                            else
                                _error4 "The URL of the repo does not exist"
                            fi
                        fi
                    fi
                fi

                unset MOD_BRANCH
                unset MOD_URL
                unset MOD_NAME
                unset REPO_DEST
            done < "$_DEPENDENCIES_PATH"
        else
            _error3 "$_DESTINATION_PATH does not exist"
        fi
    else
        _error3 "$_DEPENDENCIES_PATH does not exist"
    fi
    unset _FORCE
    unset _PULL
    unset _DEPENDENCIES_PATH
    unset _DESTINATION_PATH
}

function _generate_addons_path {
    _DEPENDENCIES_PATH=$1
    _ADDONS_TMP_PATH_FILE=$2

    _log2 "Generate Addons path $_DEPENDENCIES_PATH to $_ADDONS_TMP_PATH_FILE"

    _TMP_PATH_FILE=$_DEPENDENCIES_PATH/.addons_path.txt
    
    cd $_DEPENDENCIES_PATH
    echo "" > $_TMP_PATH_FILE

    find $PWD -name "__manifest__.py*" | while read filepath; do
        #_log3 "Processing file '$filepath'"
        parent="$(dirname "$filepath")"
        #_log4 "Parent '$parent'"
        grand="$(dirname "$parent")"
        #_log4 "grand '$grand'"
        echo $grand >> "$_TMP_PATH_FILE"
    done
    sort -u $_TMP_PATH_FILE -o $_TMP_PATH_FILE
    cat $_TMP_PATH_FILE >> $_ADDONS_TMP_PATH_FILE
    rm $_TMP_PATH_FILE

    #data=$(while read line
    #    do
    #        echo -n "${line},"
    #    done < $_TMP_PATH_FILE)
    unset _DEPENDENCIES_PATH
    unset _ADDONS_TMP_PATH_FILE
    unset _TMP_PATH_FILE 
    _success3 "Generated addons path: $data"
}

function _map_odoo_user_with_target_uid {
    _TARGET_UID=$1
    _ODOO_USER=$2

    _log2 "Map Odoo user with target UID..."

    # Name of the target Odoo user
    TARGET_USER_NAME='target-odoo-user'

    # Check whether target user exists or not
    exists=$( getent passwd "$_TARGET_UID" | wc -l )

    # Create target user
    if [ "$exists" == "0" ]; then
        _log3 'Target user does not exist, Odoo user is now the target Odoo user'
        ODOO_USER="$TARGET_USER_NAME"

        _log3 'Creating target Odoo user...'
        adduser --uid "$_TARGET_UID" --disabled-login --gecos "" --quiet "$ODOO_USER"

        _log3 'Add target user to odoo group so that he can read/write the content of /opt/odoo'
        usermod -a -G odoo "$ODOO_USER"

        _success3 "Mapped Odoo user with target UID"
    else
        _log3 'Check whether trying to map with the same UID as odoo user'
        ODOO_USER_ID=$( id -u "$_ODOO_USER" )

        if [ "$_TARGET_UID" -ne "$ODOO_USER_ID" ]; then

            _log3 'Check whether trying to map with an existing user other than the target user'
            TARGET_UID_NAME=$( getent passwd "$_TARGET_UID" | cut -d: -f1 )

            if [ "$TARGET_USER_NAME" != "$TARGET_UID_NAME" ]; then
                _error3 'Cannot create target user as target UID already exists.'
            else
                 _log3 'User already created, container was only restarted'
                ODOO_USER="$TARGET_USER_NAME"
                _success3 "Mapped Odoo user with target UID"
            fi
        else
            _success3 "Mapped Odoo user with target UID"
        fi
    fi
}
