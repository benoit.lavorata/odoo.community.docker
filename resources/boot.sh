#!/bin/bash
source /usr/bin/helpers.sh
SCRIPT_NAME="Odoo container - Boot script"
SCRIPT_DESCRIPTION="Setup conf, download addons, boot odoo"
SCRIPT_AUTHOR="Benoit Lavorata"

function start {
    _log "start"
    _br

    ODOO_USER='odoo'
    ODOO_CONF_FILE_PATH="/opt/odoo/etc/odoo.conf"
    ODOO_STARTUP_SCRIPT_PATH="/opt/scripts/startup.sh"
    ODOO_PREBOOT_SCRIPT_PATH="/opt/scripts/preboot.sh"
    DEPENDENCIES_PATH="/opt/odoo/git-src/"
    MARKETPLACE_PATH="/opt/odoo/marketplace-src/"
    DEPENDENCIES_FILE_PATH="/opt/odoo/local-src/oca_dependencies.txt"
    MARKETPLACE_FILE_PATH="/opt/odoo/local-src/marketplace_dependencies.txt"
    REQUIREMENTS_FILE_PATH="/opt/odoo/local-src/requirements.txt"
    ADDONS_FILE_PATH="/opt/odoo/local-src/addons_path.txt"
    CURRENT_FOLDER="/opt/odoo/"


    if [ -d $MARKETPLACE_PATH ]; then
        cd $CURRENT_FOLDER
    else
        mkdir $MARKETPLACE_PATH
    fi

    if [ -d $DEPENDENCIES_PATH ]; then
        cd $CURRENT_FOLDER
    else
        mkdir $DEPENDENCIES_PATH
    fi


    # Host user mapping
    # Check if needed to map host user with container odoo user
    if [ "$TARGET_UID" ]; then
        _log1 "TARGET_UID is set to $TARGET_UID, mapping with user $ODOO_USER is needed"
        _map_odoo_user_with_target_uid $TARGET_UID $ODOO_USER
    else
        _info2 "TARGET_UID not set, no mapping is needed"
    fi
    _br


    # If the folders mapped to the volumes didn't exist, Docker has created
    # them with root instead of the target user. Making sure to give back the
    # ownership to the corresponding host user.
    _log1 "Check volumes access rights"
    _ensure_odoo_user_owns_volume /opt/odoo/etc $ODOO_USER
    _ensure_odoo_user_owns_volume /opt/odoo/marketplace-src $ODOO_USER
    _ensure_odoo_user_owns_volume /opt/odoo/git-src $ODOO_USER
    _ensure_odoo_user_owns_volume /opt/odoo/local-src $ODOO_USER
    _ensure_odoo_user_owns_volume /opt/odoo/data $ODOO_USER
    _ensure_odoo_user_owns_volume /opt/odoo/data/filestore $ODOO_USER
    _ensure_odoo_user_owns_volume /opt/odoo/data/sessions $ODOO_USER
    _ensure_odoo_user_owns_volume /opt/odoo/data/addons $ODOO_USER
    _ensure_odoo_user_owns_volume /opt/odoo/ssh $ODOO_USER
    _br


    if [ -f $ODOO_STARTUP_SCRIPT_PATH ]; then
        _log1 "Execute startup script $ODOO_STARTUP_SCRIPT_PATH"

        _hr
        bash $ODOO_STARTUP_SCRIPT_PATH
        _hr
        _success2 "Executed startup script"
    else
        _info2 "no startup script found at $ODOO_STARTUP_SCRIPT_PATH, skip"
    fi
    _br


    _log1 "Set git user info and ssh keys"
    sudo -i -u "$ODOO_USER" git config --global user.email "contact@docker.com"
    sudo -i -u "$ODOO_USER" git config --global user.name "Odoo Docker"
    if [ -f /opt/odoo/ssh/id_rsa ]; then
        _setup_ssh_key $ODOO_USER
    else
        _info2 "no startup script found at $ODOO_STARTUP_SCRIPT_PATH, skip"
    fi
    _br

    # Check if need to download addons repo
    _log1 "Check if need to download oca addons repo"
    if [ -f $DEPENDENCIES_FILE_PATH ]; then
        cd $CURRENT_FOLDER
        _download_git_addons $DEPENDENCIES_FILE_PATH $DEPENDENCIES_PATH
    else
        _info2 "no dependencies file found at $DEPENDENCIES_FILE_PATH, skip"
    fi
    _br


    if [ -f $MARKETPLACE_FILE_PATH ]; then
        cd $CURRENT_FOLDER
        _download_marketplace_addons $MARKETPLACE_FILE_PATH $MARKETPLACE_PATH
    else
        _info2 "no dependencies file found at $MARKETPLACE_FILE_PATH, skip"
    fi
    _br


    # GENEREATE ADDONS_PATH
    _log1 "Generate adddons path"
    ADDONS_PATH=""
    TMP_ADDONS_FILE=${CURRENT_FOLDER}.addons_path.txt
    echo "/opt/odoo/git-src" >> $TMP_ADDONS_FILE
    echo "/opt/odoo/marketplace-src" >> $TMP_ADDONS_FILE
    echo "/opt/odoo/local-src" >> $TMP_ADDONS_FILE

    cp $TMP_ADDONS_FILE $TMP_ADDONS_FILE.backup
    while read line
    do
    [ -z "$line" ] && continue
       _generate_addons_path $line $TMP_ADDONS_FILE
    done < $TMP_ADDONS_FILE.backup
    rm $TMP_ADDONS_FILE.backup

    echo "/opt/odoo/sources/odoo/addons" >> $TMP_ADDONS_FILE

    if [ -f $ADDONS_FILE_PATH ]; then
        _log1 "Additional adddons path from ${ADDONS_FILE_PATH}"
        while read line
        do
        [ -z "$line" ] && continue
            if [[ $line = \#* ]] ; then
                _log2 "Skip commented line"
            else
                echo "${line}" >> $TMP_ADDONS_FILE
                _success2 "Added ${line} to path"
                _ensure_odoo_user_owns_volume $line $ODOO_USER
            fi
            
        done < $ADDONS_FILE_PATH
    fi
    _br

    sort -u $TMP_ADDONS_FILE -o $TMP_ADDONS_FILE

    if [ -f $REQUIREMENTS_FILE_PATH ]; then
        _log1 "Install requirements from $REQUIREMENTS_FILE_PATH"
        pip3 install -r $REQUIREMENTS_FILE_PATH
        _success2 "Installed requirements (pip3)"
        #_install_requirements_recursive
    else
        _info2 "no requirements.txt found at $REQUIREMENTS_FILE_PATH, skip"
    fi
    _br

    # CONCATENATE
    export ODOO_ADDONS_PATH=$(while read line
    do
	[ -z "$line" ] && continue
        echo -n "${line},"
    done < $TMP_ADDONS_FILE)
    ODOO_ADDONS_PATH=${ODOO_ADDONS_PATH%?};
    rm $TMP_ADDONS_FILE
    
    _log2 "ODOO_ADDONS_PATH=${ODOO_ADDONS_PATH}"
    _br

    if [ -f $ODOO_PREBOOT_SCRIPT_PATH ]; then
        _log1 "Execute preboot script $ODOO_PREBOOT_SCRIPT_PATH"
        _hr
        bash $ODOO_PREBOOT_SCRIPT_PATH
        _hr
    else
        _info2 "no preboot script found at $ODOO_PREBOOT_SCRIPT_PATH, skip"
    fi
    _br

    
    #_log1 "Update odoo conf file : $ODOO_CONF_FILE_PATH"
    _update_odoo_conf_params $ODOO_CONF_FILE_PATH
    _br

    _info "Will now start Odoo process... "
    if [ ! -e $1 ]; then
        _log1 "with additional args: $*" 
    fi
    _outro
    sudo -i -u "$ODOO_USER" "$PYTHON_BIN" "/opt/odoo/sources/odoo/$SERVICE_BIN" -c "$ODOO_CONF_FILE_PATH" $*
}

# Run command

_intro
$*
