FROM registry.gitlab.com/benoit.lavorata/odoo.base.docker:13.0
MAINTAINER Benoit Lavorata

User odoo
ADD resources /opt/odoo/resources

User 0
RUN  set -x; \
  chmod -R 775 /opt/odoo \
  && mv /opt/odoo/resources/marketplace_dependencies.txt /opt/odoo/local-src \
  && mv /opt/odoo/resources/oca_dependencies.txt /opt/odoo/local-src \
  && mv /opt/odoo/resources/requirements.txt /opt/odoo/local-src \
  && mv /opt/odoo/resources/helpers.sh /usr/bin/helpers.sh \
  && mv /opt/odoo/resources/boot.sh /usr/bin/boot.sh \
  && chmod +x /usr/bin/boot.sh \
  && mv /opt/odoo/resources/startup.sh /opt/scripts/startup.sh \
  && chmod +x /opt/scripts/startup.sh \
  && chown -R odoo:odoo /opt/odoo \
  && mv /opt/odoo/resources/pip.txt /opt/odoo/pip.txt \
  && pip3 install -r /opt/odoo/pip.txt \
  && apt-get remove -y postgresql-client \
  && echo 'deb http://apt.postgresql.org/pub/repos/apt/ stretch-pgdg main' > /etc/apt/sources.list.d/pgdg.list \
  && curl -sSL https://www.postgresql.org/media/keys/ACCC4CF8.asc | apt-key add - \
  && apt-get update  \
  && apt-get install -y postgresql-client \
  && pip3 install paramiko


# Startup script for custom setup
ENTRYPOINT [ "/usr/bin/dumb-init", "/usr/bin/boot.sh" ]
CMD [ "help" ]
EXPOSE 8069 8072
